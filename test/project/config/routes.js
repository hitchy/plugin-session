"use strict";

exports.routes = {
	"/"( req, res ) {
		res.json( {
			counter: ( req.session && req.session.counter ) || 0,
		} );
	},
	"/increase"( req, res ) {
		res.json( {
			counter: req.session.counter = ( req.session.counter || 0 ) + 1, // eslint-disable-line no-param-reassign
		} );
	},
	"/alt/decrease"( req, res ) {
		res.json( {
			counter: req.session.counter = ( req.session.counter || 0 ) - 1, // eslint-disable-line no-param-reassign
		} );
	},
	"/custom/read"( req, res ) {
		this.service.Session.getStore().load( req.session.id )
			.then( record => {
				res.json( {
					anyValue: {
						simulated: req.session.anyValue,
						actual: record.anyValue,
					},
					"any:Va.lue": {
						simulated: req.session["any:Va.lue"],
						actual: record["any:Va.lue"],
					}
				} );
			} )
			.catch( error => res.status( 500 ).json( { error: error.message } ) );
	},
	"/custom/write"( req, res ) {
		req.session.anyValue = "some-value"; // eslint-disable-line no-param-reassign
		req.session["any:Va.lue"] = "another-value"; // eslint-disable-line no-param-reassign

		res.json( { success: true } );
	},
};
