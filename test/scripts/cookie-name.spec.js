"use strict";

const { describe, before, after, it } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );

require( "should" );
require( "should-http" );


describe( "Hitchy instance with plugin for server-side sessions using custom cookie name", () => {
	const ctx = {};
	let sid = null;

	before( SDT.before( ctx, {
		plugin: true,
		projectFolder: "../project",
		files: {
			"config/session-alt.js": "exports.session = {cookieName: 'foo'};"
		}
	} ) );

	after( SDT.after( ctx ) );

	it( "is running", () => {
		return ctx.get( "/" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
			} );
	} );

	it( "is returning cookie for selecting server-side session", () => {
		return ctx.get( "/" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.headers.should.have.property( "set-cookie" ).which.is.an.Array();

				res.headers["set-cookie"].some( cookie => {
					const match = /^\s*sessionid=([^;]+)/i.exec( cookie );
					if ( match ) {
						sid = match[1];
					}

					return Boolean( match );
				} ).should.be.false();

				( sid == null ).should.be.true();

				res.headers["set-cookie"].some( cookie => {
					const match = /^\s*foo=([^;]+)/.exec( cookie );
					if ( match ) {
						sid = match[1];
					}

					return Boolean( match );
				} ).should.be.true();

				sid.should.be.String().which.is.not.empty();
			} );
	} );

	it( "is returning same cookie with different value on repeating query without providing recently returned cookie", () => {
		return ctx.get( "/" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.headers.should.have.property( "set-cookie" ).which.is.an.Array();

				let newSid;

				res.headers["set-cookie"].some( cookie => {
					const match = /^\s*sessionid=([^;]+)/i.exec( cookie );
					if ( match ) {
						newSid = match[1];
					}

					return Boolean( match );
				} ).should.be.false();

				res.headers["set-cookie"].some( cookie => {
					const match = /^\s*foo=([^;]+)/.exec( cookie );
					if ( match ) {
						newSid = match[1];
					}

					return Boolean( match );
				} ).should.be.true();

				newSid.should.be.ok().and.be.String().which.is.not.equal( sid );
			} );
	} );

	it( "is returning same cookie with different value on repeating query with session ID provided in cookie named sessionId (which is ignored)", () => {
		return ctx.get( "/", { cookie: `sessionId=${sid}` } )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.headers.should.have.property( "set-cookie" ).which.is.an.Array();

				let newSid;

				res.headers["set-cookie"].some( cookie => {
					const match = /^\s*sessionid=([^;]+)/i.exec( cookie );
					if ( match ) {
						newSid = match[1];
					}

					return Boolean( match );
				} ).should.be.false();

				res.headers["set-cookie"].some( cookie => {
					const match = /^\s*foo=([^;]+)/.exec( cookie );
					if ( match ) {
						newSid = match[1];
					}

					return Boolean( match );
				} ).should.be.true();

				newSid.should.be.ok().and.be.String().which.is.not.equal( sid );
			} );
	} );

	it( "is NOT returning same cookie on repeating query with session ID provided in selected custom cookie", () => {
		return ctx.get( "/", { cookie: `foo=${sid}` } )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.headers.should.not.have.property( "set-cookie" );
			} );
	} );

	it( "is returning some adjusted value of freshly initialized counter", () => {
		return ctx.get( "/increase" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.ownProperty( "counter" ).which.is.a.Number().and.equal( 1 );
			} );
	} );

	it( "is returning same adjusted value of another freshly initialized counter due to starting another session on every request", () => {
		return ctx.get( "/increase" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.ownProperty( "counter" ).which.is.a.Number().and.equal( 1 );
			} );
	} );

	it( "is returning same adjusted value of freshly initialized counter on selecting previously created session using cookie returned before", () => {
		sid.should.be.ok();

		return ctx.get( "/increase", {
			cookie: `foo=${sid}`,
		} )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.ownProperty( "counter" ).which.is.a.Number().and.equal( 1 );
			} );
	} );

	it( "is re-using previously fetched counter value due on selecting existing server-side session", () => {
		sid.should.be.ok();

		return ctx.get( "/increase", {
			cookie: `foo=${sid}`,
		} )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.ownProperty( "counter" ).which.is.a.Number().and.equal( 2 );
			} );
	} );

	it( "is redirecting attempts to write any property on session to session.$data", async() => {
		sid.should.be.ok();

		const write = await ctx.get( "/custom/write", {
			cookie: `foo=${sid}`,
		} );

		write.should.have.status( 200 );
		write.should.have.contentType( "application/json" );
		write.data.success.should.be.true();

		const read = await ctx.get( "/custom/read", {
			cookie: `foo=${sid}`,
		} );

		read.should.have.status( 200 );
		read.should.have.contentType( "application/json" );
		read.data.should.be.an.Object().which.has.size( 2 ).and.has.properties( "anyValue", "any:Va.lue" );
		read.data.anyValue.should.be.an.Object().which.has.size( 2 ).and.has.properties( "simulated", "actual" );
		read.data["any:Va.lue"].should.be.an.Object().which.has.size( 2 ).and.has.properties( "simulated", "actual" );

		read.data.anyValue.simulated.should.equal( "some-value" );
		read.data.anyValue.actual.should.equal( "some-value" );

		read.data["any:Va.lue"].simulated.should.equal( "another-value" );
		read.data["any:Va.lue"].actual.should.equal( "another-value" );
	} );
} );
