"use strict";

const OS = require( "node:os" );
const Crypto = require( "node:crypto" );
const Path = require( "node:path" );
const File = require( "node:fs" ); // eslint-disable-line no-redeclare

const { describe, before, after, it } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );

require( "should" );

const backupFolder = Path.resolve( OS.tmpdir(), Crypto.webcrypto.randomUUID() );

describe( "MemorySessionStore", () => {
	const ctx = {};
	let sid;

	before( SDT.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	after( SDT.after( ctx ) );

	it( "can be created", () => {
		( () => new ctx.hitchy.api.service.MemorySessionStore() ).should.not.throw();
	} );

	it( "provides API for creating, reading and writing properties of a session with a unique session ID", async() => {
		const memory = new ctx.hitchy.api.service.MemorySessionStore();

		sid = await memory.create();

		sid.should.be.ok().and.be.a.String();

		await memory.save( sid, { bar: "FOO", baz: 123 } );

		const record = await memory.load( sid );

		record.should.be.an.Object().which.has.size( 2 ).and.has.properties( "bar", "baz" );
		record.bar.should.equal( "FOO" );
		record.baz.should.equal( 123 );
	} );

	it( "garbage-collects records that have not been read for a configurable amount of time", async() => {
		const memory = new ctx.hitchy.api.service.MemorySessionStore( { maxAge: 0.3 } ); // garbage collect after 300ms

		// create the record
		sid = await memory.create();

		await memory.save( sid, { bar: "FOO", baz: 123 } );

		// read session after a moment
		await new Promise( resolve => setTimeout( resolve, 50 ) );

		const first = await memory.load( sid );

		first.should.be.an.Object().which.has.size( 2 ).and.has.properties( "bar", "baz" );
		first.bar.should.equal( "FOO" );
		first.baz.should.equal( 123 );

		// read session again after some time assuming previous read has reset its age
		await new Promise( resolve => setTimeout( resolve, 270 ) );

		const second = await memory.load( sid );

		second.should.be.an.Object().which.has.size( 2 ).and.has.properties( "bar", "baz" );
		second.bar.should.equal( "FOO" );
		second.baz.should.equal( 123 );

		// read session again after its maximum age has been exceeded to see it's gone
		await new Promise( resolve => setTimeout( resolve, 400 ) );

		const third = await memory.load( sid );

		( third == null ).should.be.true();
	} );
} );

describe( "Persisting MemorySessionStore", function() {
	this.timeout( 5000 );

	const ctx = {};
	let sid;

	before( () => File.promises.mkdir( backupFolder, { recursive: true } ) );
	before( SDT.before( ctx, {
		plugin: true,
		projectFolder: false,
		files: {
			"config/all.js": `exports.session = { backupFolder: "${backupFolder.replace( /\\/g, "\\\\" )}" };`,
		},
	} ) );

	after( SDT.after( ctx ) );
	after( () => File.promises.rm( backupFolder, { recursive: true, force: true } ) );

	it( "can be enabled using custom configuration to survive a restart of Hitchy", async() => {
		// configuration is correct
		ctx.hitchy.api.config.session.backupFolder.should.equal( backupFolder );

		// use the store before shutdown
		const first = new ctx.hitchy.api.service.MemorySessionStore();
		sid = await first.create();

		await first.save( sid, { foo: "major", bar: 123, baz: { name: "baz" } } );

		// shutdown hitchy
		await SDT.after( ctx )();

		// restart Hitchy
		await SDT.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.js": `exports.session = { backupFolder: "${backupFolder.replace( /\\/g, "\\\\" )}" };`,
			},
		} )();

		// use same session as before
		const second = new ctx.hitchy.api.service.MemorySessionStore();
		const record = await second.load( sid );

		record.should.be.an.Object().which.has.size( 3 ).and.has.properties( "foo", "bar", "baz" );
		record.foo.should.equal( "major" );
		record.bar.should.equal( 123 );
		record.baz.should.deepEqual( { name: "baz" } );
	} );
} );
