"use strict";

const { describe, before, after, it } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );

require( "should" );
require( "should-http" );


describe( "A hitchy instance server-side sessions disabled in configuration", () => {
	const ctx = {};

	after( SDT.after( ctx ) );
	before( SDT.before( ctx, {
		plugin: true,
		projectFolder: "../project",
		files: {
			"config/session-alt.js": "exports.session = {disable: true};"
		}
	} ) );

	it( "is running", () => {
		return ctx.get( "/" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
			} );
	} );

	it( "is NOT returning cookie for selecting server-side session", () => {
		return ctx.get( "/" )
			.then( res => {
				res.should.have.status( 200 );
				res.should.have.contentType( "application/json" );
				res.headers.should.not.have.property( "set-cookie" );
			} );
	} );
} );
