"use strict";

const { describe, before, after, it } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );

require( "should" );

describe( "Session", () => {
	const ctx = {};

	before( SDT.before( ctx, {
		plugin: true,
		projectFolder: "../project",
	} ) );

	after( SDT.after( ctx ) );

	it( "is exposed as service", () => {
		ctx.hitchy.api.service.Session.should.be.ok();
	} );

	it( "can be created", () => {
		( () => new ctx.hitchy.api.service.Session( "foo" ) ).should.not.throw();
	} );

	it( "provides its ID", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.id.should.equal( "foo" );
	} );

	it( "provides its ID readonly", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		( () => { session.id = "bar"; } ).should.throw();
	} );

	it( "accepts writing custom properties", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.foo = "bar";
	} );

	it( "accepts reading custom properties written before", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.foo = "bar";
		session.foo.should.equal( "bar" );
	} );

	it( "provides its data as $data", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.$data.should.deepEqual( {} );
	} );

	it( "provides its data readonly", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		( () => { session.$data = { foo: "bar" }; } ).should.throw();
	} );

	it( "accepts assignment of a user object", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.user = { uuid: "123" };
	} );

	it( "requires assigned user to be an object", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		( () => { session.user = "123"; } ).should.throw();
		( () => { session.user = 123; } ).should.throw();
		( () => { session.user = true; } ).should.throw();
		( () => { session.user = false; } ).should.throw();
		( () => { session.user = () => {}; } ).should.throw(); // eslint-disable-line no-empty-function
	} );

	it( "requires assigned user to contain a truthy UUID at least", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		( () => { session.user = {}; } ).should.throw();
		( () => { session.user = { uuid: null }; } ).should.throw();
		( () => { session.user = { uuid: undefined }; } ).should.throw();
		( () => { session.user = { uuid: "" }; } ).should.throw();
		( () => { session.user = { uuid: false }; } ).should.throw();
		( () => { session.user = { uuid: 0 }; } ).should.throw();
	} );

	it( "rejects to replace previously assigned user object with an object addressing different user by UUID", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.user = { uuid: "123" };

		( () => { session.user = { uuid: "345" }; } ).should.throw();
	} );

	it( "accepts to replace previously assigned user object with a different object addressing same user by ID", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.user = { uuid: "123", name: "foo" };

		( () => { session.user = { uuid: "123", name: "bar" }; } ).should.not.throw();
	} );

	it( "deeply exposes assigned user as readonly/frozen", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.user = { uuid: "123", name: "foo" };

		( () => { session.user.uuid = "345"; } ).should.throw();
		( () => { session.user.name = "bar"; } ).should.throw();
	} );

	it( "rejects to drop previously assigned user object", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		session.user = { uuid: "123" };

		( () => { session.user = null; } ).should.throw();
		( () => { session.user = undefined; } ).should.throw();
		( () => { session.user = false; } ).should.throw();
		( () => { session.user = {}; } ).should.throw();
		( () => { session.user = ""; } ).should.throw();
		( () => { session.user = 0; } ).should.throw();
	} );

	it( "can be wrapped", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );

		( () => ctx.hitchy.api.service.Session.wrap( session ) ).should.not.throw();
		( () => session.constructor.wrap( session ) ).should.not.throw();
	} );

	it( "forwards write access on custom properties to its $data when wrapped", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );
		const wrapped = session.constructor.wrap( session );

		wrapped.foo = "bar";

		session.$data.should.deepEqual( { foo: "bar" } );
		wrapped.$data.should.deepEqual( { foo: "bar" } );
	} );

	it( "forwards read access on custom properties to its $data when wrapped", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );
		const wrapped = session.constructor.wrap( session );

		wrapped.foo = "bar";

		wrapped.foo.should.equal( "bar" );
	} );

	it( "emits event on session when writing custom properties while wrapped", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );
		const wrapped = session.constructor.wrap( session );
		let captured = null;

		session.once( "data-changed", path => { captured = path; } );

		wrapped.foo = "bar";

		captured.should.deepEqual( ["foo"] );
	} );

	it( "emits event on session when writing property of custom properties while wrapped", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );
		const wrapped = session.constructor.wrap( session );
		let captured = null;

		wrapped.foo = { bar: "baz" };

		session.once( "data-changed", path => { captured = path; } );

		wrapped.foo.bar = "bam";

		captured.should.deepEqual( [ "foo", "bar" ] );
	} );

	it( "does not emit event on session when reading custom properties while wrapped", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );
		const wrapped = session.constructor.wrap( session );
		let captured = null;

		wrapped.foo = "bar";

		session.once( "data-changed", path => { captured = path; } );

		const read = wrapped.foo; // eslint-disable-line no-unused-vars

		( captured == null ).should.be.true();
	} );

	it( "does not emit event on session when reading property of custom properties while wrapped", () => {
		const session = new ctx.hitchy.api.service.Session( "foo" );
		const wrapped = session.constructor.wrap( session );
		let captured = null;

		wrapped.foo = { bar: "baz" };

		session.once( "data-changed", path => { captured = path; } );

		const read = wrapped.foo.bar; // eslint-disable-line no-unused-vars

		( captured == null ).should.be.true();
	} );
} );
