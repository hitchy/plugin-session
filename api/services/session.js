"use strict";

const { EventEmitter } = require( "node:events" );

let store;

/**
 * Validates format of session IDs.
 *
 * @type {RegExp}
 */
const PtnSessionId = /^[a-z0-9_/+-]{16}$/i;

module.exports = function() {
	const api = this;

	/**
	 * Implements session management.
	 */
	class Session extends EventEmitter {
		#user;

		/**
		 * @param {string} sessionId unique ID of session
		 * @param {Object<string,any>} properties copy (!) of session's properties recovered from a session store
		 * @param {boolean} frozen if true, the session's ID, user and data set are frozen and can not be re-configured e.g. by an inheriting class
		 */
		constructor( sessionId, properties, frozen = true ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes ID of session.
				 *
				 * @name Session#id
				 * @property {string}
				 * @readonly
				 */
				id: { value: sessionId, configurable: !frozen },

				/**
				 * Exposes authenticated user current session is associated with.
				 *
				 * @name Session#user
				 * @property {{uuid:string}}
				 */
				user: {
					get: () => this.#user,
					set: newUser => {
						if ( !newUser || typeof newUser !== "object" || !newUser.uuid ) {
							throw new Error( "invalid user descriptor rejected" );
						}

						if ( this.#user != null ) {
							if ( newUser.uuid === this.#user.uuid ) {
								return;
							}

							throw new Error( "invalid request for replacing user of current session" );
						}

						this.#user = Object.freeze( { ...newUser } );
					},
					configurable: !frozen,
				},

				/**
				 * Exposes space for saving additional custom data in context of
				 * current session.
				 *
				 * @name Session#$data
				 * @property {object}
				 * @readonly
				 */
				$data: { value: properties || {}, configurable: !frozen },
			} );
		}

		/**
		 * Fetches store managing sessions' records in a potentially persistent
		 * data source.
		 *
		 * @returns {MemorySessionStore} session store to use
		 */
		static getStore() {
			if ( !store ) {
				if ( api.config.session.store instanceof api.service.MemorySessionStore ) {
					store = api.config.session.store;
				} else if ( api.config.session.store ) {
					throw new Error( "invalid session store configuration" );
				} else {
					store = new api.service.MemorySessionStore();
				}
			}

			return store;
		}

		/**
		 * Provides configured name of session cookie recognized by the session
		 * management.
		 *
		 * @returns {string} name of cookie
		 */
		static getCookieName() {
			return api.config.session.cookieName || "sessionId";
		}

		/**
		 * Provides value of configured session cookie available in provided
		 * request.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req descriptor of request to read cookie value from
		 * @returns {null|string} session ID as found in value of configured cookie of provided request
		 */
		static getSessionIdOfRequest( req ) {
			return req.cookies[this.getCookieName()];
		}

		/**
		 * Basically validates provided session ID.
		 *
		 * The method does not check whether a session for the provided ID
		 * exists or not. It solely checks the format of the provided ID to
		 * mitigate opportunities for attack.
		 *
		 * @param {string} id ID of a session to be validated
		 * @returns {boolean} true if provided ID is a valid one, false e.g. if it does not comply with constraints on a session ID's format
		 */
		static isValidSessionId( id ) {
			return typeof id === "string" && PtnSessionId.test( id );
		}

		/**
		 * Adjusts provided response manager to promote current session's ID so
		 * that the client is using it in follow-up requests. Usually this is
		 * based on setting a cookie.
		 *
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @returns {void}
		 */
		$promoteSessionIdInResponse( res ) {
			const parts = [
				`${this.constructor.getCookieName()}=${this.id}`,
				`Path=${api.config.session.cookiePath || "/"}`,
				`SameSite=${api.config.session.cookieSameSite || "Strict"}`,
				"HttpOnly",
			];

			const maxAge = Number( api.config.session.cookieMaxAge ) || 0;
			if ( maxAge > 0 ) {
				parts.push( `MaxAge=${maxAge}` );
			}

			const secure = api.utility.value.asBoolean( api.config.session.cookieSecure );
			if ( secure === true ) {
				parts.push( "Secure" );
			}

			const partitioned = api.utility.value.asBoolean( api.config.session.cookiePartitioned );
			if ( partitioned === true ) {
				parts.push( "Partitioned" );
			}

			res.set( "Set-Cookie", parts.join( "; " ) );
		}

		/**
		 * Wraps provided session to simplify access on its data and track write
		 * access on it to be reported by emitting events on the session.
		 *
		 * @param {Session} session session to be wrapped
		 * @returns {Session} wrapped session
		 */
		static wrap( session ) {
			if ( !( session instanceof Session ) ) {
				throw new TypeError( "wrapping requires Session instance" );
			}

			return new Proxy( session, {
				get( target, property ) {
					switch ( property ) {
						case "id" :
						case "user" :
							return target[property];

						default :
							if ( /^\$/.test( property ) || property in Object.getPrototypeOf( target ) ) {
								const value = target[property];

								if ( typeof value === "function" ) {
									return value.bind( target );
								}

								return value;
							}

							return monitorWriteAccess( target.$data[property], path => {
								session.emit( "data-changed", [ property, ...path ] );
							} );
					}
				},
				set( target, property, value ) {
					switch ( property ) {
						case "id" :
							return false;

						case "user" :
							target[property] = value; // eslint-disable-line no-param-reassign
							return true;

						default :
							if ( /^\$/.test( property ) || property in Object.getPrototypeOf( target ) ) {
								return false;
							}

							target.$data[property] = value; // eslint-disable-line no-param-reassign

							session.emit( "data-changed", [property] );

							return true;
					}
				},
			} );
		}
	}

	return Session;
};

/**
 * Optionally wraps object provided as value in a proxy reporting write access
 * on its properties through provided callback and keeps wrapping objects found
 * as properties of provided value in the same way.
 *
 * @param {any} value value to be wrapped for monitoring properties for write access
 * @param {function(path: string[]):void} onChangeFn callback invoked with a sequence of properties names selecting the eventually changed value
 * @returns {object} provided non-object value as-is, proxy wrapping some provided object
 */
function monitorWriteAccess( value, onChangeFn ) {
	if ( typeof value !== "object" || !value ) {
		return value;
	}

	return new Proxy( value, {
		get( target, property ) {
			return monitorWriteAccess( target[property], path => onChangeFn( [ property, ...path ] ) );
		},
		set( target, property, newValue ) {
			target[property] = newValue;

			onChangeFn( [property] );

			return true;
		}
	} );
}
