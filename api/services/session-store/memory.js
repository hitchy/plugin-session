"use strict";

const Crypto = require( "node:crypto" );
const Path = require( "node:path" );
const File = require( "node:fs" ); // eslint-disable-line no-redeclare

module.exports = function() {
	const api = this;

	const logDebug = api.log( "hitchy:session:memory:debug" );
	const logError = api.log( "hitchy:session:memory:error" );

	/**
	 * Implements store for sessions to be persisted between requests in current
	 * runtime.
	 *
	 * The API of this class is prepared to work with different stores as well.
	 * Custom stores must inherit from this class.
	 */
	class MemorySessionStore {
		/**
		 * Pool of current sessions.
		 *
		 * @type {Map<string,SessionRecord>}
		 */
		#sessions = new Map();

		/**
		 * Controls maximum age of a session in seconds before it gets
		 * garbage-collected.
		 *
		 * @type {number}
		 */
		#maxAgeInSeconds = 3600;

		#gcTimer;

		/**
		 * Caches promise set on construction to be resolved once a found backup
		 * file has been recovered so that using the session store gets deferred
		 * accordingly.
		 */
		#recovered;

		/**
		 * On shutting down Hitchy, plugin-session is deferring its shutdown
		 * until this promise has been settled. It is used internally to make
		 * sure the file for writing a configured backup has been written.
		 *
		 * @type {Promise<void>}
		 */
		static shutdown = Promise.resolve();

		/**
		 * @param {number} maxAge maximum number of seconds a session is kept at least without being accessed
		 */
		constructor( { maxAge = 3600 } = {} ) {
			this.#maxAgeInSeconds = Number( maxAge ) || 3600;

			// set up regular garbage collection
			this.#gcTimer = setInterval( () => this.gc(), 10000 );

			const { backupFolder } = api.config.session || {};
			const backupFilename = backupFolder ? Path.resolve( backupFolder, "session.json" ) : undefined;

			api.once( "shutdown", () => {
				// release garbage collection schedule
				clearInterval( this.#gcTimer );

				// optionally save session records to local file
				if ( backupFilename ) {
					this.constructor.shutdown = this.constructor.shutdown
						.then( () => this.backup() )
						.then( records => File.promises.writeFile( backupFilename, JSON.stringify( records ), "utf-8" ) )
						.then( () => {
							logDebug( "memory store backup has been written to", backupFilename );
						} );
				}
			} );

			if ( backupFilename ) {
				// optionally recover session records from a local file
				logDebug( "recovering memory-based session store from backup file", backupFilename );

				this.#recovered = File.promises.readFile( backupFilename, "utf-8" )
					.then( file => this.restore( JSON.parse( file ) ) )
					.catch( cause => {
						if ( cause.code === "ENOENT" ) {
							logDebug( "backup file is missing" );
						} else {
							logError( "recovering session from backup failed:", cause.stack );
						}
					} );
			} else {
				this.#recovered = Promise.resolve();
			}
		}

		/**
		 * Requests to garbage-collect outdated sessions.
		 *
		 * @returns {Promise<void>} promise resolved when garbage collection is done
		 */
		async gc() { // eslint-disable-line require-await
			await this.#recovered; // eslint-disable-line no-empty-function

			const threshold = Date.now() - this.#maxAgeInSeconds * 1000;

			for ( const [ id, entry ] of this.#sessions.entries() ) {
				if ( entry.touched < threshold ) {
					this.#sessions.delete( id );
				}
			}
		}

		/**
		 * Generates new record for a session associating it with a unique ID.
		 *
		 * @param {number} maxAttempts maximum number of attempts to pick a new random session ID before giving up
		 * @returns {Promise<string>} promise resolved with unique ID of session which had been made room for in store
		 */
		async create( maxAttempts = 100 ) { // eslint-disable-line require-await
			await this.#recovered; // eslint-disable-line no-empty-function

			for ( let i = 0; i < maxAttempts; i++ ) {
				const sessionId = Crypto.webcrypto.getRandomValues( Buffer.alloc( 12 ) ).toString( "base64" );

				if ( !this.#sessions.has( sessionId ) ) {
					this.#sessions.set( sessionId, {
						touched: Date.now(),
						record: "{}",
					} );

					return sessionId;
				}
			}

			throw new Error( "finding unique session ID has failed after multiple attempts" );
		}

		/**
		 * Writes provided data of session selected by its unique ID to store.
		 *
		 * @param {string} sessionId unique ID of session to load
		 * @returns {Promise<null|Object<string,any>>} promise resolved with found session's properties or null if no such record exists
		 */
		async load( sessionId ) { // eslint-disable-line require-await
			await this.#recovered; // eslint-disable-line no-empty-function

			const entry = this.#sessions.get( sessionId );

			if ( entry ) {
				const now = Date.now();
				const threshold = now - this.#maxAgeInSeconds * 1000;

				if ( entry.touched >= threshold ) {
					entry.touched = now;

					return this.deserialize( entry.record );
				}

				logDebug( "ignoring outdated session", sessionId );

				this.#sessions.delete( sessionId );
			}

			return null;
		}

		/**
		 * Writes provided data of session selected by its unique ID to store.
		 *
		 * @param {string} sessionId unique ID of session
		 * @param {Object<string,any>} record session's properties
		 * @returns {Promise<void>} resolved when the session has been saved
		 */
		async save( sessionId, record ) { // eslint-disable-line require-await
			await this.#recovered; // eslint-disable-line no-empty-function

			const entry = this.#sessions.get( sessionId );

			if ( entry ) {
				entry.touched = Date.now();
				entry.record = this.serialize( record );
			} else {
				logError( "invalid attempt for saving unknown session %s", sessionId );
			}
		}

		/**
		 * Removes data on session selected by its ID from this store.
		 *
		 * @param {string} sessionId ID of session to drop
		 * @returns {Promise<void>} promise resolved when session has been dropped
		 */
		async drop( sessionId ) { // eslint-disable-line require-await
			await this.#recovered; // eslint-disable-line no-empty-function

			this.#sessions.delete( sessionId );
		}

		/**
		 * Converts session's set of properties into a serialized record
		 * suitable for storing it persistently.
		 *
		 * @param {Object<string,any>} properties set of properties to serialize
		 * @returns {string|Object<string,any>} serialized record of provided properties
		 */
		serialize( properties ) {
			return properties && typeof properties === "object" ? JSON.stringify( properties ) : "";
		}

		/**
		 * Converts session's set of properties from a serialized record read
		 * from a persistent store.
		 *
		 * @param {string|Object<string,any>} record serialized record of a session's properties
		 * @returns {Object<string,any>} properties of session
		 */
		deserialize( record ) {
			return record && typeof record === "string" ? JSON.parse( record ) : undefined;
		}

		/**
		 * Runs garbage collection and retrieves list of all session records
		 * surviving it.
		 *
		 * @returns {Promise<{id: string, item: {touched: number, record: string}}[]>} list of session records
		 */
		async backup() {
			await this.gc();

			const count = this.#sessions.size;
			const records = new Array( count );
			let write = 0;

			for ( const [ id, item ] of this.#sessions.entries() ) {
				records[write++] = { id, item };
			}

			return records;
		}

		/**
		 * Recovers session records from a previously made backup.
		 *
		 * @param {{id: string, item: {touched: number, record: string}}[]} backup backup to restore
		 * @returns {Promise<void>} promise resolved when backup has been restored
		 */
		async restore( backup ) { // eslint-disable-line require-await
			if ( Array.isArray( backup ) ) {
				const threshold = Date.now() - this.#maxAgeInSeconds * 1000;

				for ( const { id, item } of backup ) {
					if ( id && typeof id === "string" &&
					     item && typeof item === "object" &&
					     item.touched > threshold &&
					     typeof item.record === "string" && item.record.trim() !== ""
					) {
						this.#sessions.set( id, item );
					}
				}
			}
		}
	}

	return MemorySessionStore;
};

/**
 * @typedef {object} SessionRecord
 * @property {number} touched number in milliseconds since last time this session has been used
 * @property {string} record serialized properties of session
 */
