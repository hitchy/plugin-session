"use strict";

module.exports = function() {
	const api = this;
	const { service } = api;

	const logDebug = api.log( "hitchy:session:injector:debug" );

	/**
	 * Implements code for setting up a session in context of a given request
	 * using an ID with that request.
	 */
	class SessionInjector {
		/**
		 * Injects session selected by its ID found in a cookie included with
		 * provided request into that request.
		 *
		 * The request's cookies are looked up for containing a session ID to
		 * restore that session from storage and inject it into given session.
		 * The separately provided response manager is used to set headers for
		 * either setting the cookie in case a new session has been started and
		 * for including a custom indicator on proper injection of a session.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @returns {Promise<void>} promise for having injected a session into the request
		 */
		static async injectIntoRequest( req, res ) {
			const { Session, HttpException } = service;

			// reject to process injection twice per request
			if ( req.session ) {
				throw new HttpException( 400, "unexpected session in request" );
			}

			const store = Session.getStore();
			let sessionId = Session.getSessionIdOfRequest( req );
			let session;

			logDebug( `injecting session with cookie-based session ID: ${sessionId}` );

			if ( sessionId ) {
				if ( !Session.isValidSessionId( sessionId ) ) {
					throw new HttpException( 400, "invalid session ID" );
				}

				const record = await store.load( sessionId );

				if ( record ) {
					session = new Session( sessionId, record );
				} else {
					logDebug( `missing session with ID ${sessionId}` );
				}
			}

			if ( session ) {
				logDebug( `found session with ID ${sessionId}` );

				if ( Number( api.config.session.cookieMaxAge ) > 0 ) {
					// session cookie is set up for expiration
					// -> keep refreshing its maximum age while in use
					session.$promoteSessionIdInResponse( res );
				}
			} else {
				sessionId = await store.create();
				session = new Session( sessionId, await store.load( sessionId ) );

				logDebug( `creating new with ID ${session.id}` );

				session.$promoteSessionIdInResponse( res );
			}

			Object.defineProperties( req, {
				session: { value: session.constructor.wrap( session ) },
			} );

			if ( !res.headersSent ) {
				res.set( "X-Have-Session", "true" );
			}
		}
	}

	return SessionInjector;
};
