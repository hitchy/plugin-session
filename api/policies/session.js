"use strict";

module.exports = function() {
	const api = this;
	const { service } = api;

	const logDebug = api.log( "hitchy:session:debug" );
	const logError = api.log( "hitchy:session:error" );

	/**
	 * Implements a set of policies for associating requests with a server-side
	 * session.
	 */
	class SessionPolicy {
		/**
		 * Injects session selected by its ID given in a request cookie into the request
		 * descriptor.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next callback invoked when handler is done
		 * @returns {void}
		 */
		static inject( req, res, next ) {
			service.SessionInjector.injectIntoRequest( req, res )
				.then( next )
				.catch( cause => {
					logError( `on handling session injection: ${cause.stack}` );

					next( cause );
				} );
		}

		/**
		 * Injects session selected by its ID given in a request cookie into the request
		 * descriptor.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next callback invoked when handler is done
		 * @returns {void}
		 */
		static async persist( req, res, next ) {
			if ( req.session?.id ) {
				logDebug( `persisting session with ID ${req.session.id} in store` );

				await api.service.Session.getStore().save( req.session.id, req.session.$data );
			}

			next();
		}
	}

	return SessionPolicy;
};
