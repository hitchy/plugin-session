"use strict";

/**
 * Defines defaults for session configuration.
 */
exports.session = {
	// Provides instance of custom store to use instead of MemorySessionStore.
	store: null,

	// If true, policy for integrating session with every incoming request is not
	// injected.
	disable: false,

	// Selects name of cookie to use for identifying either request's related
	// server-side session.
	cookieName: "sessionId",

	// Provides value of `Path` option on setting the session cookie.
	cookiePath: "/",

	// Requests to mark session cookies to expire after given number of seconds.
	// When omitted or 0, session cookie is set without any expiration other
	// than user closing the browser.
	cookieMaxAge: 0,

	// Controls value of `SameSite` option on setting the session cookie.
	cookieSameSite: "Strict",

	// Controls whether session cookie is set with `Secure` option or not.
	cookieSecure: false,

	// Controls whether session cookie is set with `Partitioned` option or not.
	cookiePartitioned: false,
};
