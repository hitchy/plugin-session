# @hitchy/plugin-session [![pipeline status](https://gitlab.com/hitchy/plugin-session/badges/master/pipeline.svg)](https://gitlab.com/hitchy/plugin-session/-/commits/master)


## License

[MIT](LICENSE)

## Usage

In your [Hitchy-based](http://hitchy.org/) application run this command

```bash
npm install --save @hitchy/plugin-session @hitchy/plugin-cookies
```

to install this plugin. 

> As illustrated above you are in charge of installing _@hitchy/plugin-cookies_ which is a mandatory dependency for this plugin. So, if you get an error regarding some unmet dependency on a role _cookies_ you might need to repeat this command exactly as given.

After restarting your Hitchy-based application it is discovering this plugin and instantly injecting policy for selecting server-side session by ID provided in cookie passed with every request. Usually, this is done by browsers implicitly. Any request missing cookie with session's ID causes start of another session and injecting instructions for saving cookie in response.

On server side the session is exposed in `req.session`. It consists of multiple properties:

* `user` is provided to expose name and roles of some user. Managing current user is basically out of this plugin's scope. See hitchy-plugin-auth for that.
  * `user.name` is expected to be a string containing name of current user.
  * `user.roles` is a list of roles current user is authorized for.
* `data` is an object prepared to hold arbitrary data.

## Configuration

The plugin does not require any configuration to work out of the box. It may be customized via section `session` of your runtime configuration, though.

Create a file adjusting configuration section `session` in **<your-project>/config/session.js** with content like this:

```javascript
exports.session = {
	// TODO add your session configuration here ...
};
```

Currently, these options are supported:

* `disable` is a _boolean-ish_ value. Set it `true` to disable automatic injection of session management into all incoming requests. The default is `false`.

  > A `boolean-ish` value is `true`, `false` or any string representing either value such as `on` vs. `off` or `yes` vs. `no`.

* `store` can be used to provide a custom store. When omitted, a default store is saving sessions in runtime memory of running Hitchy application.

  > No additional stores come included with this plugin. Custom stores need to inherit from the default store exposed at runtime as `api.service.MemorySessionStore`.

* `backupFolder` selects a folder used by the default store for writing its records to a file **session.json** in that folder on shutting down Hitchy. On starting Hitchy, that file is read by the default store to recover a previous set of sessions. This feature is meant to establish real persistence of sessions with the default store otherwise using volatile runtime memory, only.

* `cookieName` is a string selecting the name of cookie considered to provide a session's ID in incoming requests. The default is `"sessionId"`.

* `cookiePath` controls the [path prefix](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#pathpath-value) a client browser is requested to provide session cookie on. It is `/` to cover all incoming requests by default.

* `cookieMaxAge` is [the number of seconds after which the cookie is expiring](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#max-agenumber). It is `0` by default preventing the cookie from expiring prior to the browser's session being closed.

  When set, a request for updating the session cookie is included with every incoming request involving the server-side session management.

* `cookieSameSite` controls the value of the [`SameSite` option](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#samesitesamesite-value) on setting the session cookie. It is `Strict` by default. Other options include `None` and `Lax`.

* `cookieSecure` is a _boolean-ish_ option controlling the value of the [`Secure` option](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#secure) on setting the session cookie.  It is `false` by default.

* `cookiePartitioned` is a _boolean-ish_ option controlling the value of the [`Partitioned` option](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#partitioned) on setting the session cookie. It is `false` by default.
