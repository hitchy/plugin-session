"use strict";

exports.policies = function() {
	const disable = this.utility.value.asBoolean( this.config.session.disable );

	return disable === true ? {} : {
		before: {
			"ALL /": "session::inject",
		},
		after: {
			"ALL /": "session::persist",
		}
	};
};

exports.shutdown = async function() {
	await this.service.MemorySessionStore.shutdown;
};
